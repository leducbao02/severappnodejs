import userController from "./user.js";
import bookController from "./book.js";

export  {
    userController,
    bookController
}