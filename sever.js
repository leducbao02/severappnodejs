import express from 'express'
import *as dotenv from 'dotenv' 
import  {userRouter,bookRouter} from './routers/index.js'
dotenv.config()
const app = express()
const port = process.env.PORT ?? 3000

app.use('/user', userRouter)
app.use('/books', bookRouter)
app.get('/', (req,res)=>{
      res.send('respone from root router, haha')
})

app.listen(port, async() =>{
   console.log(`listening on port: ${port}`)
})
