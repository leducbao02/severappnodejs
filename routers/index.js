import bookRouter from './books.js'
import userRouter from './user.js'


export {
    userRouter,
    bookRouter
}
